//! This tool converts strings into aws safe strings

use std::io::prelude::*;
use std::io::{self, BufReader};
use regex::{Regex, Captures};

/// Convert an base 10 integer into base 26 alpha characters
///
/// ```
/// let result = num_to_alpha(123);
/// assert_eq!(result, "te")
/// ```
fn num_to_alpha(num: u64) -> String {
    let mut remaining = num;
    let mut out: Vec<u8> = vec![];
    
    let a: u8 = 97;
    while remaining > 0 {
        let remainder = a + ((remaining % 26) as u8);
        out.push(remainder);
        remaining /= 26;
    }

    out.iter().map(|c| char::from(*c)).collect()
}

fn main() -> io::Result<()> {
    let reader = BufReader::new(io::stdin());

    let number_re = Regex::new(r"\d+").unwrap();

    for line in reader.lines() {
        let text = line?;
        
        let out = number_re.replace_all(&text, |caps: &Captures| {
            let num: u64 = caps.get(0).unwrap().as_str().parse().unwrap();
            num_to_alpha(num)
        });

        println!("{}", out.to_ascii_lowercase());
    }

    Ok(())
}
