# Safe-aws-name
Ensure a string is safe with AWS' naming rules.

## Example
```shell
# echo "SomeServer-Name-123" | aws-safe-name
someserver-name-te
```
